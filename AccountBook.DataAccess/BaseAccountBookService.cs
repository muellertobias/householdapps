﻿using System;
using System.Collections.Generic;
using System.Linq;
using HouseholdApps.AccountBook.Core;

namespace AccountBook.DataAccess
{
    public abstract class BaseAccountBookService : IAccountBookService
    {
        private IList<Purchase> _purchases;

        private Lazy<IList<Purchase>> purchases => new Lazy<IList<Purchase>>(() => Inititalize());

        public event AccountBookUpdated PurchaseAdded;
        public event AccountBookUpdated PurchaseDeleted;
        public event AccountBookUpdated PurchaseUpdated;


        private IList<Purchase> _Purchases
        {
            get
            {
                if (_purchases == null)
                {
                    _purchases = purchases.Value;
                }
                return _purchases;
            }
        }

        protected abstract IList<Purchase> Inititalize();

        public void Add(Purchase purchase)
        {
            if (_Purchases.All(p => p.Guid != purchase.Guid))
            {
                _Purchases.Add(purchase.Clone());
                PurchaseAdded?.Invoke(this, new AccountBookEventArgs(purchase));
            }
        }

        protected void AddWithoutNotification(Purchase purchase)
        {
            if (_Purchases.All(p => p.Guid != purchase.Guid))
            {
                _Purchases.Add(purchase.Clone());
            }
        }

        public void Delete(Purchase purchase)
        {
            if (_Purchases.Any(p => p.Guid == purchase.Guid))
            {
                _Purchases.Remove(purchase);
                PurchaseDeleted?.Invoke(this, new AccountBookEventArgs(purchase));
            }
        }

        public IEnumerable<Purchase> GetAll()
        {
            return _Purchases.Select(p => p.Clone());
        }

        public IEnumerable<Purchase> GetPurchasesFromMonthOf(DateTime date)
        {
            if (date == default)
            {
                return GetAll();
            }
            return GetAll().Where(p => p.Date.Month == date.Month);
        }

        public void Update(Purchase purchase)
        {
            var savedPurchase = _Purchases.SingleOrDefault(p => p.Guid == purchase.Guid);
            if (savedPurchase != null)
            {
                if (!ReferenceEquals(savedPurchase, purchase))
                {
                    savedPurchase.Date = purchase.Date;
                    savedPurchase.Note = purchase.Note;
                    savedPurchase.Price_Euro = purchase.Price_Euro;
                    savedPurchase.Reason = purchase.Reason;

                    PurchaseUpdated?.Invoke(this, new AccountBookEventArgs(purchase.Clone()));
                }
            }
        }

        public IEnumerable<string> GetCommonPurchaseReasons()
        {
            return _Purchases.Select(p => p.Reason);
        }
    }
}
