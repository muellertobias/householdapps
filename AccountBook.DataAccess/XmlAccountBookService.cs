﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;
using HouseholdApps.AccountBook.Core;

namespace AccountBook.DataAccess
{
    public class XmlAccountBookService : BaseAccountBookService
    {
        private readonly string filename = "purchases.xml";
        private readonly XmlSerializer serializer = new XmlSerializer(typeof(PurchasesList));

        public XmlAccountBookService()
        {
            PurchaseUpdated += XmlAccountBookService_PurchaseUpdated;
        }

        private void XmlAccountBookService_PurchaseUpdated(object sender, AccountBookEventArgs eventArgs)
        {
            var list = new PurchasesList(GetAll());
            using (var stream = new FileStream(filename, FileMode.OpenOrCreate))
            {
                serializer.Serialize(stream, list);
            }
        }

        protected override IList<Purchase> Inititalize()
        {
            if (File.Exists(filename))
            {
                using (var stream = new FileStream(filename, FileMode.Open))
                {
                    return TryDeserialize(stream);
                }
            }
            else
            {
                return new PurchasesList();
            }
        }

        private IList<Purchase> TryDeserialize(FileStream stream)
        {
            try
            {
                return serializer.Deserialize(stream) as PurchasesList;
            }
            catch
            {
                stream.Close();
                File.Move(filename, $"Invalid_{filename}_{DateTime.Now.Ticks}");
                return new PurchasesList();
            }
        }
    }
}
