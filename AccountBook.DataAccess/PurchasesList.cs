﻿using System.Collections.Generic;
using System.Linq;
using HouseholdApps.AccountBook.Core;

namespace AccountBook.DataAccess
{
    public class PurchasesList : List<Purchase> 
    {
        public decimal Costs_Euro
        {
            get => this.Sum(p => p.Price_Euro);
            set { }
        }

        public PurchasesList(IEnumerable<Purchase> collection)
            : base(collection)
        {
        }

        public PurchasesList() { }
    }
}
