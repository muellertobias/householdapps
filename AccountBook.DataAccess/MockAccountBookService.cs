﻿using System;
using System.Collections.Generic;
using HouseholdApps.AccountBook.Core;

namespace AccountBook.DataAccess
{
    public class MockAccountBookService : BaseAccountBookService
    {

        public MockAccountBookService(IEnumerable<Purchase> purchases)
        {
            foreach (var p in purchases)
            {
                AddWithoutNotification(p);
            }
        }

        protected override IList<Purchase> Inititalize()
        {
            return new List<Purchase>()
            {
                new Purchase() { Date = DateTime.Now, Price_Euro = 10, Reason = "Lebensmittel" },
                new Purchase() { Date = DateTime.Now.AddMonths(-1), Price_Euro = 15.50m, Reason = "Sonstiges" }
            };  
        }
    }
}
