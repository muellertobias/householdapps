﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Markup;

namespace HouseholdApps.AccountBook.UI
{
    /// <summary>
    /// Interaktionslogik für "App.xaml"
    /// </summary>
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            FrameworkElement.LanguageProperty.OverrideMetadata(typeof(FrameworkElement), 
                new FrameworkPropertyMetadata(XmlLanguage.GetLanguage(
                    CultureInfo.CurrentCulture.IetfLanguageTag)));
            base.OnStartup(e);
            EventManager.RegisterClassHandler(typeof(TextBox), TextBox.GotKeyboardFocusEvent, new RoutedEventHandler(TextBox_GotKeyboardFocus));
        }

        private void TextBox_GotKeyboardFocus(object sender, RoutedEventArgs e)
        {
            var textBox = sender as TextBox;
            var eventArgs = e as KeyboardFocusChangedEventArgs;
            if (textBox != null && eventArgs != null)
            {
                if (eventArgs.KeyboardDevice.IsKeyDown(Key.Tab))
                {
                    textBox.SelectAll();
                }
            }
        }
    }
}
