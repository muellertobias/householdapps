﻿using AccountBook.DataAccess;
using HouseholdApps.WPFUtilities;

namespace HouseholdApps.AccountBook.UI.ViewModels
{
    class MainViewModel : NotificationBase
    {
        public AccountBookViewModel AccountBook { get; }

        public MainViewModel()
        {
            AccountBook = new AccountBookViewModel(new XmlAccountBookService());
        }
    }
}
