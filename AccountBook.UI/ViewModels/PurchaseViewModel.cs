﻿using HouseholdApps.AccountBook.Core;
using System;
using HouseholdApps.WPFUtilities;

namespace HouseholdApps.AccountBook.UI.ViewModels
{
    public class PurchaseViewModel : ViewModelBase<Purchase>
    {
        private readonly IAccountBookService accountBookService;

        public string Reason
        {
            get => Model.Reason;
            set => SetValue(v => Model.Reason = v, v => Model.Reason != v, value);
        } 

        public decimal Price_Euro
        {
            get => Model.Price_Euro;
            set => SetValue(v => Model.Price_Euro = v, v => Model.Price_Euro != v, value);
        }
        
        public DateTime Date
        {
            get => Model.Date;
            set => SetValue(v => Model.Date = v, v => Model.Date != v, value);
        } 

        public string Note
        {
            get => Model.Note;
            set => SetValue(v => Model.Note = v, v => Model.Note != v, value);
        }

        public PurchaseViewModel(Purchase model, IAccountBookService accountBookService) 
            : base(model)
        {
            this.accountBookService = accountBookService ?? throw new ArgumentNullException(nameof(accountBookService));
            PropertyChanged += (s, e) => Update();
        }

        public void Update()
        {
            accountBookService.Update(Model);
        }

        public void DeleteFromAccountBook()
        {
            accountBookService.Delete(Model);
        }

        public override bool Equals(object obj)
        {
            if (obj == null)
            {
                return false;
            }

            if (ReferenceEquals(this, obj))
            {
                return true;
            }

            if (obj is PurchaseViewModel)
            {
                return Equals((obj as PurchaseViewModel).Model);
            }
            else if (obj is Purchase)
            {
                return Model.Guid == (obj as Purchase).Guid;
            }
            else
            {
                return false;
            }
        }

        public override int GetHashCode()
        {
            var hashCode = -1275310383;
            hashCode = hashCode * -1521134295 + Model.GetHashCode();
            return hashCode;
        }
    }
}
