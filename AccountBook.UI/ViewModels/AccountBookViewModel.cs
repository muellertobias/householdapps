﻿using HouseholdApps.AccountBook.Core;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Windows.Input;
using HouseholdApps.WPFUtilities;
using System.Windows.Data;
using System.Collections.Specialized;

namespace HouseholdApps.AccountBook.UI.ViewModels
{
    public class AccountBookViewModel : ViewModelBase<IAccountBookService>
    {
        private readonly ObservableCollection<PurchaseViewModel> PurchaseViewModels;
        private decimal _outgoings;
        private DateTime _selectedMonth;

        public DateTime SelectedMonth
        {
            get => _selectedMonth;
            set => SetValue(v => _selectedMonth = v, value);
        }

        public ICollectionView PurchasesView { get; }

        public PurchaseViewModel SelectedPurchase
        {
            get => PurchasesView.CurrentItem as PurchaseViewModel;
            set => SetValue(v => PurchasesView.MoveCurrentTo(v), value);
        }

        public decimal Outgoings
        {
            get => _outgoings;
            private set => SetValue(v => _outgoings = v, value);
        }

        public ICommand AddPurchaseCmd { get; }
        public ICommand UpdatePurchaseCmd { get; }
        public ICommand DeletePurchaseCmd { get; }
        public ICommand GetPurchasesFromMonthOfDateCmd { get; }

        public AccountBookViewModel(IAccountBookService model)
            : base(model)
        {
            SelectedMonth = DateTime.Now;
            PurchaseViewModels = new ObservableCollection<PurchaseViewModel>();
            PurchasesView = CollectionViewSource.GetDefaultView(PurchaseViewModels);
            PurchasesView.CurrentChanged += (s, e) => RaisePropertyChanged(nameof(SelectedPurchase));
            PurchasesView.CollectionChanged += PurchasesView_CollectionChanged;
            GetPurchasesFromMonthOf(SelectedMonth);

            Model.PurchaseAdded += PurchaseHasBeenAdded;
            Model.PurchaseDeleted += PurchaseHasBeenDeleted;
            Model.PurchaseUpdated += PurchaseHasBeenUpdated;

            AddPurchaseCmd = new RelayCommand<PurchaseViewModel>(_ => AddPurchase());
            UpdatePurchaseCmd = new RelayCommand<PurchaseViewModel>(viewModel => viewModel.Update(), viewModel => viewModel != null);
            DeletePurchaseCmd = new RelayCommand<PurchaseViewModel>(viewModel => viewModel.DeleteFromAccountBook(), viewModel => viewModel != null);
            GetPurchasesFromMonthOfDateCmd = new RelayCommand<DateTime>(date => GetPurchasesFromMonthOf(date), date => date != null);

            PropertyChanged += AccountBookViewModel_PropertyChanged;
        }

        private void AccountBookViewModel_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(SelectedMonth))
            {
                GetPurchasesFromMonthOfDateCmd?.Execute(_selectedMonth);
            }
        }

        private void PurchaseHasBeenAdded(object sender, AccountBookEventArgs eventArgs)
        {
            var viewModel = new PurchaseViewModel(eventArgs.Purchase, Model);
            PurchaseViewModels.Add(viewModel);
            SelectedPurchase = viewModel;
        }

        private void PurchaseHasBeenDeleted(object sender, AccountBookEventArgs eventArgs)
        {
            var viewModel = PurchaseViewModels.Where(vm => vm.Equals(eventArgs.Purchase)).FirstOrDefault();
            if (viewModel != null)
            {
                PurchaseViewModels.Remove(viewModel);
            }
        }

        private void PurchasesView_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            Outgoings = PurchaseViewModels.Sum(p => p.Price_Euro);
        }

        private void PurchaseHasBeenUpdated(object sender, AccountBookEventArgs e)
        {
            var viewModel = PurchaseViewModels.Where(vm => vm.Equals(e.Purchase)).FirstOrDefault();
            // Update ViewModel
            Outgoings = PurchaseViewModels.Sum(p => p.Price_Euro);
        }

        private void GetPurchasesFromMonthOf(DateTime date)
        {
            if (SelectedMonth != date)
            {
                SelectedMonth = date;
            }

            PurchaseViewModels.Clear();
            var purchases = Model.GetPurchasesFromMonthOf(date);
            foreach (var purchase in purchases)
            {
                PurchaseViewModels.Add(new PurchaseViewModel(purchase, Model));
            }
            Outgoings = purchases.Sum(p => p.Price_Euro);
        }

        private void AddPurchase()
        {
            var p = new Purchase();
            Model.Add(p);
        }
    }
}
