﻿using System.Collections.Generic;
using System.Linq;

namespace HouseholdApps.AccountBook.Core
{
    public class BookingService : IBookingService
    {
        public decimal CalculateCosts(IEnumerable<Purchase> purchases)
        {
            return purchases.Sum(p => p.Price_Euro);
        }

        public decimal CalculateCostsFor(string reason, IEnumerable<Purchase> purchases)
        {
            return purchases.Where(p => p.Reason == reason).Sum(p => p.Price_Euro);
        }
    }
}
