﻿using System;
using System.Collections.Generic;

namespace HouseholdApps.AccountBook.Core
{
    public class Purchase : IEquatable<Purchase>
    {
        public Guid Guid { get; set; } 
        public string Reason { get; set; } 
        public decimal Price_Euro { get; set; } 
        public DateTime Date { get; set; }
        public string Note { get; set; } 

        public Purchase()
            : this(Guid.NewGuid(), string.Empty, 0m, DateTime.Now, string.Empty)
        {
        }

        public Purchase(Guid guid, string reason, decimal price_Euro, DateTime date, string note)
        {
            Guid = guid;
            Reason = reason ?? throw new ArgumentNullException(nameof(reason));
            Price_Euro = price_Euro;
            Date = date;
            Note = note ?? throw new ArgumentNullException(nameof(note));
        }

        public override bool Equals(object obj)
        {
            return Equals(obj as Purchase);
        }

        public bool Equals(Purchase other)
        {
            return other != null &&
                   Guid.Equals(other.Guid) &&
                   Reason == other.Reason &&
                   Price_Euro == other.Price_Euro &&
                   Date == other.Date &&
                   Note == other.Note;
        }

        public override int GetHashCode()
        {
            var hashCode = 1687656317;
            hashCode = hashCode * -1521134295 + EqualityComparer<Guid>.Default.GetHashCode(Guid);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Reason);
            hashCode = hashCode * -1521134295 + Price_Euro.GetHashCode();
            hashCode = hashCode * -1521134295 + Date.GetHashCode();
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Note);
            return hashCode;
        }

        public static bool operator ==(Purchase purchase1, Purchase purchase2)
        {
            return EqualityComparer<Purchase>.Default.Equals(purchase1, purchase2);
        }

        public static bool operator !=(Purchase purchase1, Purchase purchase2)
        {
            return !(purchase1 == purchase2);
        }

        public Purchase Clone()
        {
            return new Purchase(Guid, Reason, Price_Euro, Date, Note);
        }
    }
}
