﻿using System;

namespace HouseholdApps.AccountBook.Core
{
    public class AccountBookEventArgs : EventArgs
    {
        public Purchase Purchase { get; }

        public AccountBookEventArgs(Purchase purchase)
        {
            Purchase = purchase ?? throw new ArgumentNullException(nameof(purchase));
        }
    }
}
