﻿using System.Collections.Generic;

namespace HouseholdApps.AccountBook.Core
{
    public interface IBookingService
    {
        decimal CalculateCosts(IEnumerable<Purchase> purchases);
        decimal CalculateCostsFor(string reason, IEnumerable<Purchase> purchases);
    }
}
