﻿using System;
using System.Collections.Generic;

namespace HouseholdApps.AccountBook.Core
{
    public delegate void AccountBookUpdated(object sender, AccountBookEventArgs eventArgs);

    public interface IAccountBookService
    {
        event AccountBookUpdated PurchaseAdded;
        event AccountBookUpdated PurchaseDeleted;
        event AccountBookUpdated PurchaseUpdated;

        void Add(Purchase purchase);
        IEnumerable<Purchase> GetAll();
        IEnumerable<Purchase> GetPurchasesFromMonthOf(DateTime date);
        void Update(Purchase purchase);
        void Delete(Purchase purchase);
        IEnumerable<string> GetCommonPurchaseReasons();
    }
}
