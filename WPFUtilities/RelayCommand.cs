﻿using System;
using System.Windows.Input;

namespace HouseholdApps.WPFUtilities
{
    public class RelayCommand<T> : ICommand
    {
        private readonly Action<T> command;
        private readonly Func<T, bool> canExecute;

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public RelayCommand(Action<T> command)
            : this(command, _ => true) { }

        public RelayCommand(Action<T> command, Func<T, bool> canExecute)
        {
            this.command = command ?? throw new ArgumentNullException(nameof(command));
            this.canExecute = canExecute ?? throw new ArgumentNullException(nameof(canExecute));
        }

        public bool CanExecute(object parameter)
        {
            return canExecute(Cast(parameter));
        }

        private static T Cast(object parameter)
        {
            return (parameter == null) ? default : (T)Convert.ChangeType(parameter, typeof(T));
        }

        public void Execute(object parameter)
        {
            command(Cast(parameter));
        }
    }
}
