﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace HouseholdApps.WPFUtilities
{
    public abstract class NotificationBase : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        protected void SetValue<T>(Action<T> setter, Func<T, bool> canSet, T newValue, [CallerMemberName]string propertyName = "")
        {
            if (canSet(newValue))
            {
                setter(newValue);
                RaisePropertyChanged(propertyName);
            }
        }

        protected void SetValue<T>(Action<T> setter, T newValue, [CallerMemberName]string propertyName = "")
        {
            SetValue(setter, _ => true, newValue, propertyName);
        }

        protected void RaisePropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
