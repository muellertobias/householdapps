﻿using System;
using System.ComponentModel;

namespace HouseholdApps.WPFUtilities
{
    public abstract class ViewModelBase<TModel> : NotificationBase
    {
        [Bindable(false)]
        protected readonly TModel Model;

        public ViewModelBase(TModel model)
        {
            if (model == null)
            {
                throw new ArgumentNullException(nameof(model));
            }
            Model = model;
        }
    }
}
