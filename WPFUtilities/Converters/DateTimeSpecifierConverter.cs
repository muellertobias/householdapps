﻿using System;
using System.Globalization;

namespace HouseholdApps.WPFUtilities.Converters
{
    public enum DayOfMonth
    {
        NotSpecified = 0,
        First,
        Last
    }

    public class DateTimeSpecifierConverter : ConverterMarkupExtension
    {
        public override object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is DateTime date)
            {
                if (parameter is DayOfMonth dayOfMonth)
                {
                    switch (dayOfMonth)
                    {
                        case DayOfMonth.First:
                            return new DateTime(date.Year, date.Month, 1);
                        case DayOfMonth.Last:
                            return new DateTime(date.Year, date.Month, DateTime.DaysInMonth(date.Year, date.Month));
                        default:
                            return date;
                    }
                }
            }
            return null;
        }

        public override object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
