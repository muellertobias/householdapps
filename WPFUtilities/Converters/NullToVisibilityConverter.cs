﻿using System;
using System.Globalization;
using System.Windows;

namespace HouseholdApps.WPFUtilities.Converters
{
    public sealed class NullToVisibilityConverter : ConverterMarkupExtension
    {
        public override object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value == null ? Visibility.Hidden : Visibility.Visible;
        }

        public override object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotSupportedException();
        }
    }
}
