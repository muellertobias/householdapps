﻿using System;
using System.Globalization;

namespace HouseholdApps.WPFUtilities.Converters
{
    public sealed class NullToFalseConverter : ConverterMarkupExtension
    {
        public override object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value != null;
        }

        public override object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotSupportedException();
        }
    }
}
